We'll create a unit file to manage the MongoDB service. Create a configuration file named mongodb.service in the /etc/systemd/system directory using nano or your favorite text editor.

> sudo nano /etc/systemd/system/mongodb.service
Paste in the following contents, then save and close the file.

#/etc/systemd/system/mongodb.service

		[Unit]
		Description=High-performance, schema-free document-oriented database
		After=network.target

		[Service]
		User=mongodb
		ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf

		[Install]
		WantedBy=multi-user.target




This file has a simple structure:


> sudo systemctl start mongodb

While there is no output to this command, you can also use systemctl to check that the service has started properly.

> sudo systemctl status mongodb

		Output
		● mongodb.service - High-performance, schema-free document-oriented database
		   Loaded: loaded (/etc/systemd/system/mongodb.service; enabled; vendor preset: enabled)
		   Active: active (running) since Mon 2016-04-25 14:57:20 EDT; 1min 30s ago
		 Main PID: 4093 (mongod)
		    Tasks: 16 (limit: 512)
		   Memory: 47.1M
		      CPU: 1.224s
		   CGroup: /system.slice/mongodb.service
			   └─4093 /usr/bin/mongod --quiet --config /etc/mongod.conf

The last step is to enable automatically starting MongoDB when the system starts.

> sudo systemctl enable mongodb
